package com.example.babynamesurfer

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val DATABASE_NAME = "babynames"

    // range of years to show in the graph
    private val START_YEAR = 1980
    private val END_YEAR = 2010

    // range of ranks to show; MIN rank is 1; 0 means "not ranked"
    private val MAX_RANK = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        graph_view.viewport.setMinX(START_YEAR.toDouble())
        graph_view.viewport.setMaxX(END_YEAR.toDouble())
        graph_view.viewport.setMinY(0.0)
        graph_view.viewport.setMaxY(MAX_RANK.toDouble())

    }

    // called when the user clicks the Search button. Runs a query
    fun searchClick(view: View) {
        doQuery()
    }

    // performs a query on the babynames database
    // for the given name/sex and prints the result
    private fun doQuery() {
        val name = name_text.text.toString()
        val sex = if (male_female_switch.isChecked) "F" else "M"
        // todo: do the query on the database
        // find all the rankings for this name+sex
        val db = openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null)
        val query = "SELECT year, rank FROM ranks WHERE name = '$name' AND sex = '$sex'"
        val cr = db.rawQuery(query, null)
        val series = LineGraphSeries<DataPoint>()
        while (cr.moveToNext()) {
            val year = cr.getInt(cr.getColumnIndex("year"))
            val rank = MAX_RANK - cr.getInt(cr.getColumnIndex("rank"))
            series.appendData(DataPoint(year.toDouble(), rank.toDouble()), false, 20)
        }
        cr.close()
        graph_view.removeAllSeries()
        graph_view.addSeries(series)
    }

    private fun importDatabase(dbName: String) {
        val db = openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null)
        val resId = resources.getIdentifier(dbName, "raw", packageName)
        val scan = Scanner(resources.openRawResource(resId))
        var query = ""
        while (scan.hasNextLine()) {
            val line = scan.nextLine()
            if (line.trim().startsWith("--")) continue
            query += "$line\n"
            if (query.trim().endsWith(";")) {
                db.execSQL(query)
                query = ""
            }
        }
    }

}