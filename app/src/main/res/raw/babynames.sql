CREATE TABLE ranks (
  name varchar(32) NOT NULL DEFAULT '',
  sex  varchar(1)  NOT NULL DEFAULT 'M',
  year integer     NOT NULL DEFAULT 0,
  rank integer     NOT NULL DEFAULT 0
);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1880, 247);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1890, 227);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1900, 196);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1910, 232);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1920, 189);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1930, 88);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1940, 99);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1950, 204);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1960, 131);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1970, 401);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1980, 857);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 1990, 1584);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 2000, 2913);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Bob", "M", 2010, 8064);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1880, 1);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1890, 1);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1900, 1);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1910, 1);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1920, 1);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1930, 3);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1940, 3);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1950, 3);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1960, 4);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1970, 4);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1980, 8);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 1990, 12);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 2000, 14);
INSERT INTO ranks (name, sex, year, rank) VALUES ("John", "M", 2010, 26);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1880, 40);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1890, 57);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1900, 52);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1910, 82);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1920, 131);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1930, 132);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1940, 92);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1950, 145);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1960, 86);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1970, 220);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1980, 560);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 1990, 691);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 2000, 1303);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Tom", "M", 2010, 1818);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1880, 108);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1890, 135);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1900, 166);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1910, 226);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1920, 263);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1930, 332);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1940, 322);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1950, 218);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1960, 77);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1970, 5);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1980, 8);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 1990, 38);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 2000, 105);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Amy", "F", 2010, 136);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1880, 55);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1890, 63);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1900, 83);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1910, 111);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1920, 157);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1930, 190);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1940, 209);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1950, 225);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1960, 238);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1970, 366);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1980, 400);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 1990, 431);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 2000, 500);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Nora", "F", 2010, 159);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1880, 30);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1890, 25);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1900, 18);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1910, 16);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1920, 20);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1930, 29);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1940, 44);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1950, 69);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1960, 98);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1970, 183);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1980, 227);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 1990, 307);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 2000, 299);
INSERT INTO ranks (name, sex, year, rank) VALUES ("Rose", "F", 2010, 336);


CREATE TABLE meanings (
  name    varchar(32)  NOT NULL DEFAULT '',
  meaning varchar(512) NOT NULL DEFAULT ''
);
INSERT INTO meanings (name, meaning) VALUES ("Bob", "English, Dutch Short form of ROBERT.");
INSERT INTO meanings (name, meaning) VALUES ("John", "English, Biblical English form of Iohannes, which was the Latin form of the Greek name (Ioannes), itself derived from the Hebrew name ???????? (Yochanan) meaning 'YAHWEH is gracious'.");
INSERT INTO meanings (name, meaning) VALUES ("Tom", "English Short form of THOMAS.");
INSERT INTO meanings (name, meaning) VALUES ("Amy", "English Derived from Old French aime meaning 'beloved'.");
INSERT INTO meanings (name, meaning) VALUES ("Nora", "Irish, English, Scandinavian, German Pet form of HONORA or ELEANOR");
INSERT INTO meanings (name, meaning) VALUES ("Rose", "English, French In part it means simply 'rose' from the word for the fragrant flower (derived from Latin rosa).");
